## Description
Mongo DB handler
<br>
<br>

## How to Use

* ### Message Format 
    ```js
    /*
       Message Format  
    */

    const message = {
            'collection' : collectionName,
            'appkey'     : appkey,
            'data'        data
        }

    mqtt.Publish(request, appApi, 'mongodb', `insert/${mqData.functionName}`, mqData);
    ```

    <br>

    Sample return :
    ```js
    {   
        ...
            ...
        ]
    }
    ```