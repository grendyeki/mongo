'use strict';

require('dotenv').config({ path: `${__dirname}/config/.env` });

// local modules
const
    mqttClient  = require(`${__dirname}/modules/mqtt`),
    kafkaClient = require(`${__dirname}/modules/kafka`),
    mongoClient = require(`${__dirname}/core/mongo`),
    subscribe   = require(`${__dirname}/modules/subscriber`);



const appId = process.env.APP_ID;


const app = {
    start : () => {
        return new Promise(async (resolve, reject) => {
            const
                mqtt  = await mqttClient(),
                kafka = await kafkaClient.connect();

            resolve({ 
                mqtt  : mqtt,
                kafka : kafka
            })
        }).then((value) => { 
            global.mqtt  = value.mqtt;
            global.kafkaClient = kafkaClient;

            const mongo = mongoClient.connect();

 
            subscribe(mqtt);
        }).catch((error) => {
            console.log(error);
        });
    },

    // processHandler : () => {
    //     process.on('exit', () => {
    //         try {
    //             kafka.log('info', appId, `${appId} with PID: ${process.pid} has been manually shutdown.`);
    //         }
    //         catch(err) {
    //             console.warn(`Kafka is not connected.`)
    //             console.log(`${appId} with PID: ${process.pid} has been manually shutdown.`);
    //         }

    //         app.exit();
    //     });

    //     process.on('uncaughtException', (err) => {
    //         try {
    //             kafka.log('fatal', appId, err);
    //         }
    //         catch(err) {
    //             console.warn(`Kafka is not connected.`)
    //             console.log(err);
    //         }

    //         app.exit();
    //     });

    //     process.on('warning', (warning) => {
    //         try {
    //             kafka.log('warn', appId, warning.message);
    //         }
    //         catch(err) {
    //             console.warn(`Kafka is not connected.`);
    //             console.log(warning);
    //         }
    //     });

    //     process.on('SIGTERM', () => {
    //         try {
    //             kafka.log('info', appId, `${appId} PID: ${process.pid} terminated`);
    //         }
    //         catch(err) {
    //             console.warn(`Kafka is not connected.`);
    //             console.log(`${appId} PID: ${process.pid} terminated`);
    //         }

    //         app.exit();
    //     });

    //     process.on('SIGHUP', () => {
    //         try {
    //             kafka.log('info', appId, `${appId} PID: ${process.pid} hanged up`);
    //         }
    //         catch(err) {
    //             console.warn(`Kafka is not connected.`);
    //             console.log(`${appId} PID: ${process.pid} hanged up`);
    //         }

    //         app.exit();
    //     });

    //     process.on('SIGINT', () => {
    //         try {
    //             kafka.log('warn', appId, `${appId} PID: ${process.pid} interrupted`);
    //         }
    //         catch(err) {
    //             console.warn(`Kafka is not connected.`);
    //             console.log(`${appId} PID: ${process.pid} interrupted`);
    //         }

    //         app.exit();
    //     });
    // },

    // graceful exit
    exit : () => {
        process.exit();
        process.disconnect && process.disconnect();
    }
};

app.start();
// app.processHandler();