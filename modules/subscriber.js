/**
 * @file        : subscriber.js
 * @author      : Grendy Eki A
 * @description : Mongodb mqtt subscriber
 */

`use strict`;

module.exports = () => {
    mqtt.on('message', (topic, message) => {
        
        const 
            collection = message.collection, 
            msgData    = JSON.parse(message),
            appApi     = process.env.APP_API;

        msgData.topicName   = topic.split('/')[3];

        if(mqtt.isChunk(msgData)) {
            return;
        };

        if (topic.match(`^${appApi}\/APP[0-9]+\/insert\/[a-zA-Z0-9_-]+$`)) {
            mongo.insert(collection,msgData);
        }
        else if (topic.match(`^${appApi}\/APP[0-9]+\/update\/[a-zA-Z0-9_-]+$`)) {
            mongo.update(collection,msgData);
        }
        else if (topic.match(`^${appApi}\/APP[0-9]+\/delete\/[a-zA-Z0-9_-]+$`)) {
            mongo.delete(collection,msgData);
        }
    });
}
