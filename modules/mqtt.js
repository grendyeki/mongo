`use strict`;

const mqtt = require(`mqtt`);

module.exports = () => {
    const
        appId = process.env.APP_ID,
        host  = process.env.MQTT_HOST,
        port  = process.env.MQTT_PORT,
        topic = process.env.MQTT_TOPIC,
        url   = `${host}:${port}`;

    return new Promise((resolve, reject) => {
        const client = mqtt.connect(url, {
            clientId : appId,
            clean    : false
        });

        client.on(`connect`, () => {
            if(!topic) {
                return reject(`Topic is not define`);
            }

            client.subscribe(topic, { retain: false, qos: 1 });

            resolve(client);
        });

        client.on(`error`, (error) => {
            return reject(error);
        });
    });
};