`use strict`;

const kafka = require(`kafka-node`);

const 
    zookeper = process.env.KAFKA_ZOOKEPER,
    topic    = process.env.KAFKA_TOPIC;

const main = {
    connect : () => {
        const
            appkey    = process.env.APP_ID;

        return new Promise((resolve, reject) => {
            try {
                const 
                    client  = new kafka.KafkaClient(zookeper, appkey, {
                        sessionTimeout : 300,
                        spinDelay      : 100,
                        retries        : 2
                }, function(error){
                    if(error){
                        console.log(`error, ${appkey}, ${error}`);
                        return;
                    } else {
                        console.log(`info, ${appkey}, Service ${appkey} connected to Kafka Broker ${url}`)
                    }
                });
                resolve(client);
            }
            catch(error) {
                console.log(`error, ${appkey}, ${error}`);
            }
        }).catch((error) => {
            console.log(`critical, ${appkey}, ${error.message ? error.message : error}`);
        });
    },
    log : (level, appkey, message)=>{
        const 
            producer = new kafka.HighLevelProducer(client);
            data = {
                'level'   : level,
                'appkey'  : appkey,
                'message' : message
            },
            payload = [{
                topic     : topic,
                messages  : data.toString('utf8'),
                partition : 0
            }];
        try {
            producer.send(payload, (error,result)=>{
                if(error){
                    console.log(`error, ${appkey}, ${error.message ? error.message : error}`);
                    return;
                }

                return;
            })
        }
        catch(error){
            console.log(`error,${appkey},${error.message ? error.message : error}`);
        }
    }
};
module.exports = { ... main };